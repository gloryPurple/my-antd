export default (() => {
  switch (process.env.NO_PROXY){
    case 'mock':
      return {
        "entry": "src/index.js",
        "extraBabelPlugins": [
          "transform-runtime",
          "transform-decorators-legacy",
          "transform-class-properties",
          ["import", { "libraryName": "antd", "libraryDirectory": "es", "style": true }]
        ],
        "env": {
          "development": {
            "extraBabelPlugins": [
              "dva-hmr"
            ]
          }
        },
        "externals": {
          "g2": "G2",
          "g-cloud": "Cloud",
          "g2-plugin-slider": "G2.Plugin.slider"
        },
        "ignoreMomentLocale": true,
        "theme": "./src/theme.js",
        "hash": true
      }
      break
    case 'local':
      return {
        "entry": "src/index.js",
        "proxy": {
          "/api": {
            "target": "http://" + process.env.MY_PROXY_IP,
            "changeOrigin": true,
            "pathRewrite": { "^/api" : "" }
          }
        },
        "extraBabelPlugins": [
          "transform-runtime",
          "transform-decorators-legacy",
          "transform-class-properties",
          ["import", { "libraryName": "antd", "libraryDirectory": "es", "style": true }]
        ],
        "env": {
          "development": {
            "extraBabelPlugins": [
              "dva-hmr"
            ]
          }
        },
        "externals": {
          "g2": "G2",
          "g-cloud": "Cloud",
          "g2-plugin-slider": "G2.Plugin.slider"
        },
        "ignoreMomentLocale": true,
        "theme": "./src/theme.js",
        "hash": true
      }
      break;
  }
})();
