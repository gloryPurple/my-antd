import React from 'react';
import { Form, Input, Button, Divider } from 'antd';
import { routerRedux } from 'dva/router';
import styles from './style.less';


export default ({ formItemLayout, form, dispatch }) => {
  const { getFieldDecorator, validateFields } = form;
  const onValidateForm = () => {
    validateFields((err, values) => {
      if (!err) {
        dispatch({
          type: 'form/saveStepFormData',
          payload: values,
        });
        dispatch(routerRedux.push('/main/personalCentre/withdraw/result'));
      }
    });
  };
  return (
    <div>
      <Form layout="horizontal" className={styles.stepForm} hideRequiredMark>
        <Form.Item
          {...formItemLayout}
          label="账户余额"
        >
          <span>12349465.32(可提现:323434) 元</span>
        </Form.Item>
        <Form.Item
          {...formItemLayout}
          label="提现银行卡"
        >
          {getFieldDecorator('bankCard', {
            initialValue: '招商银行 尾号4321',
          })(
            <Input disabled={() => false} />
          )}
        </Form.Item>
        <Form.Item
          {...formItemLayout}
          label="提现金额"
        >
          {getFieldDecorator('amount', {
            rules: [
              { required: true, message: '请输入提现金额' },
              { pattern: /^(\d+)((?:\.\d+)?)$/, message: '请输入合法金额数字' },
            ],
          })(
            <Input prefix="￥" placeholder="请输入提现金额" />
          )}
        </Form.Item>
        <Form.Item
          {...formItemLayout}
          label="支付密码"
        >
          {getFieldDecorator('receiverName', {
            rules: [{ required: true, message: '请输入支付密码' }],
          })(
            <Input placeholder="请输入支付密码" />
          )}
        </Form.Item>
        <Form.Item
          wrapperCol={{
            xs: { span: 24, offset: 0 },
            sm: { span: formItemLayout.wrapperCol.span, offset: formItemLayout.labelCol.span },
          }}
          label=""
        >
          <Button type="primary" onClick={onValidateForm}>
            下一步
          </Button>
        </Form.Item>
      </Form>
      <Divider style={{ margin: '40px 0 24px' }} />
      <div className={styles.desc}>
        <h3>说明</h3>
        <h4>转账到支付宝账户</h4>
        <p>如果需要，这里可以放一些关于产品的常见问题说明。如果需要，这里可以放一些关于产品的常见问题说明。如果需要，这里可以放一些关于产品的常见问题说明。</p>
        <h4>转账到银行卡</h4>
        <p>如果需要，这里可以放一些关于产品的常见问题说明。如果需要，这里可以放一些关于产品的常见问题说明。如果需要，这里可以放一些关于产品的常见问题说明。</p>
      </div>
    </div>
  );
};
