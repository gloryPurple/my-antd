import { fakeRegister } from '../services/api';

export default {
  namespace: 'restPwd',

  state: {
    status: undefined,
  },

  effects: {
    *submit(_, { call, put }) {
      yield put({
        type: 'changeSubmitting',
        payload: true,
      });
      const response = yield call(fakeRegister);
      yield put({
        type: 'registerHandle',
        payload: response,
      });
      yield put({
        type: 'changeSubmitting',
        payload: false,
      });
    },
    *reset(_, { put }) {
      yield put({
        type: 'resetStatus',
        payload: {
          status: false,
        },
      });
    },
  },

  reducers: {
    resetStatus(state, { payload }) {
      return {
        ...state,
        status: payload.status,
      };
    },
    registerHandle(state, { payload }) {
      return {
        ...state,
        status: payload.status,
      };
    },
    changeSubmitting(state, { payload }) {
      return {
        ...state,
        submitting: payload,
      };
    },
  },
};
