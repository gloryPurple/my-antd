import dynamic from 'dva/dynamic';

// wrapper of dynamic
const dynamicWrapper = (app, models, component) =>
  dynamic({
    app,
    models: () => models.map(m => import(`../models/${m}.js`)),
    component,
  });

// nav data
export const getNavData = app => [
  {
    component: dynamicWrapper(app, ['user', 'login'], () => import('../layouts/BasicLayout')),
    layout: 'BasicLayout',
    children: [
      {
        name: '首页',
        icon: 'dashboard',
        path: 'main/HomePage/analysis',
        component: dynamicWrapper(app, ['chart'], () => import('../routes/HomePage/Analysis')),
      },
      {
        name: '商户管理',
        path: 'main/merchantManage',
        icon: 'table',
        children: [
          {
            name: '商户信息查询',
            path: 'merchant-list',
            component: dynamicWrapper(app, ['merchantList'], () => import('../routes/MerchantManage/MerchantList')),
          },
        ],
      },
      {
        name: '收支明细',
        path: 'main/dealDetail',
        icon: 'table',
        children: [
          {
            name: '支出明细查询',
            path: 'pay-list',
            component: dynamicWrapper(app, ['payList'], () => import('../routes/DealDetail/PayList')),
          },
          {
            name: '分润明细查询',
            path: 'share-list',
            component: dynamicWrapper(app, ['shareList'], () => import('../routes/DealDetail/ShareList')),
          },
        ],
      },
      {
        name: '个人中心',
        path: 'main/personalCentre',
        icon: 'form',
        children: [
          {
            name: '修改登陆密码',
            path: 'modifyLoginPwd',
            component: dynamicWrapper(app, ['modifyLoginPwd'], () => import('../routes/PersonalCentre/ModifyLoginPwd')),
          },
          {
            name: '设置支付密码',
            path: 'settingPayPwd',
            component: dynamicWrapper(app, ['settingPayPwd'], () => import('../routes/PersonalCentre/SettingPayPwd')),
          },
          {
            name: '提现到银行卡',
            path: 'withdraw',
            component: dynamicWrapper(app, ['withdraw'], () => import('../routes/Withdraw/index')),
            children: [
              {
                path: 'result',
                component: dynamicWrapper(app, ['form'], () => import('../routes/Withdraw/Step2')),
              },
            ],
          },
        ],
      },
      {
        children: [
          {
            name: '基础详情页',
            path: 'basic',
            component: dynamicWrapper(app, ['profile'], () => import('../routes/Profile/BasicProfile')),
          },
        ],
      },
    ],
  },
  {
    component: dynamicWrapper(app, [], () => import('../layouts/UserLayout')),
    layout: 'UserLayout',
    children: [
      {
        children: [
          {
            name: '登录',
            path: '',
            component: dynamicWrapper(app, ['login'], () => import('../routes/User/Login')),
          },
          {
            name: '忘记密码',
            path: 'register',
            component: dynamicWrapper(app, ['register'], () => import('../routes/User/Register')),
          },
          {
            name: '重置密码',
            path: 'resetPwd',
            component: dynamicWrapper(app, ['resetPwd'], () => import('../routes/User/ResetPwd')),
          },
          {
            name: '注册结果',
            path: 'register-result',
            component: dynamicWrapper(app, [], () => import('../routes/User/RegisterResult')),
          },
        ],
      },
    ],
  },
];
