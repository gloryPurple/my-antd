import React from 'react';
import PropTypes from 'prop-types';
import { Link, Route } from 'dva/router';
import DocumentTitle from 'react-document-title';
import { Icon } from 'antd';
import GlobalFooter from '../components/GlobalFooter';
import styles from './UserLayout.less';

const links = [];

const copyright = (
  <div>
    Copyright <Icon type="copyright" /> 宝付跨境
  </div>
);

class UserLayout extends React.PureComponent {
  static childContextTypes = {
    location: PropTypes.object,
  };

  getChildContext() {
    const { location } = this.props;
    return { location };
  }

  getPageTitle() {
    const { getRouteData, location } = this.props;
    const { pathname } = location;
    let title = '宝付国际代理商平台';
    getRouteData('UserLayout').forEach((item) => {
      if (item.path === pathname) {
        title = `${item.name} - 宝付国际代理商平台`;
      }
    });
    return title;
  }

  render() {
    const { getRouteData } = this.props;
    return (
      <DocumentTitle title={this.getPageTitle()}>
        <div className={styles.container}>
          <div className={styles.top}>
            <div className={styles.header}>
              <Link to="/">
                <span className={styles.title}>宝付国际代理商平台</span>
              </Link>
            </div>
          </div>
          {getRouteData('UserLayout').map(item => (
            <Route
              exact={item.exact}
              key={item.path}
              path={item.path}
              component={item.component}
            />
          ))}
          <GlobalFooter
            className={styles.footer}
            links={links}
            copyright={copyright}
          />
        </div>
      </DocumentTitle>
    );
  }
}

export default UserLayout;
