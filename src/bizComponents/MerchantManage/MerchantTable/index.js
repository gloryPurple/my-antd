import React, { PureComponent } from 'react';
import moment from 'moment';
import { Table } from 'antd';
import styles from './index.less';

class MerchantTable extends PureComponent {
  state = {
  };

  componentWillReceiveProps(nextProps) {
    // clean state
    if (nextProps.selectedRows.length === 0) {
      this.setState({
      });
    }
  }
  handleTableChange = (pagination, filters, sorter) => {
    this.props.onChange(pagination, filters, sorter);
  }
  render() {
    const { data: { list, pagination }, loading } = this.props;

    const merchantStatus = ['个人', '企业'];
    const accountStatus = ['关闭', '运行中', '已上线', '异常'];

    const columns = [
      {
        title: '商户号',
        dataIndex: 'no',
      },
      {
        title: '商户名',
        dataIndex: 'merchantName',
      },
      {
        title: '商户类型',
        dataIndex: 'status',
        filters: [
          {
            text: merchantStatus[0],
            value: 0,
          },
          {
            text: merchantStatus[1],
            value: 1,
          },
        ],
      },
      {
        title: '账户是否开通',
        dataIndex: 'status1',
        filters: [
          {
            text: accountStatus[0],
            value: 0,
          },
          {
            text: accountStatus[1],
            value: 1,
          },
        ],
      },
      {
        title: '手机',
        dataIndex: 'description',
      },
      {
        title: '邮箱',
        dataIndex: 'callNo',
      },
      {
        title: '注册时间',
        dataIndex: 'updatedAt',
        render: val => <span>{moment(val).format('YYYY-MM-DD HH:mm:ss')}</span>,
      },
    ];

    const paginationProps = {
      showSizeChanger: true,
      showQuickJumper: true,
      ...pagination,
    };
    return (
      <div className={styles.standardTable}>
        <Table
          loading={loading}
          rowKey={record => record.key}
          dataSource={list}
          columns={columns}
          pagination={paginationProps}
          onChange={this.handleTableChange}
        />
      </div>
    );
  }
}

export default MerchantTable;
