import React, { PureComponent } from 'react';
import moment from 'moment';
import { Table } from 'antd';
import styles from './index.less';

class PayTable extends PureComponent {
  state = {
  };

  componentWillReceiveProps(nextProps) {
    // clean state
    if (nextProps.selectedRows.length === 0) {
      this.setState({
      });
    }
  }
  handleTableChange = (pagination, filters, sorter) => {
    this.props.onChange(pagination, filters, sorter);
  }
  render() {
    const { data: { list, pagination }, loading } = this.props;

    const merchantStatus = ['个人', '企业'];
    const columns = [
      {
        title: '支出类型',
        dataIndex: 'status',
        filters: [
          {
            text: merchantStatus[0],
            value: 0,
          },
          {
            text: merchantStatus[1],
            value: 1,
          },
        ],
      },
      {
        title: '提现金额',
        dataIndex: 'merchantName',
      },
      {
        title: '提现时间',
        dataIndex: 'updatedAt',
        render: val => <span>{moment(val).format('YYYY-MM-DD HH:mm:ss')}</span>,
      },
    ];

    const paginationProps = {
      showSizeChanger: true,
      showQuickJumper: true,
      ...pagination,
    };
    return (
      <div className={styles.standardTable}>
        <Table
          loading={loading}
          rowKey={record => record.key}
          dataSource={list}
          columns={columns}
          pagination={paginationProps}
          onChange={this.handleTableChange}
        />
      </div>
    );
  }
}

export default PayTable;
